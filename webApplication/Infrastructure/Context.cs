using Microsoft.EntityFrameworkCore;
using webApplication.Infrastructure.EntityConfigurations;
using webApplication.Models;

namespace webApplication.Infrastructure
{
    public class Context : DbContext
    {
        public DbSet<User> Users { get; set; }

        public Context(DbContextOptions<Context> options): base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserConfiguration());
        }
    }
}