using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using webApplication.Models;

namespace webApplication.Infrastructure.EntityConfigurations
{
    public class UserConfiguration: IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(i => i.Id);
            builder.Property(i => i.Birthday)
                .HasColumnType("date");
            builder.Property(i => i.MobileNumber)
                .HasColumnType("nvarchar(max)")
                .HasMaxLength(20);
            builder.Property(i => i.FIO)
                .HasColumnType("nvarchar(max)")
                .HasMaxLength(100);
        }
    }
}