﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webApplication.Application.Queries
{
    public interface IUsersQueries
    {
        Task<IEnumerable<User>> GetAllUsers();
    }
}
