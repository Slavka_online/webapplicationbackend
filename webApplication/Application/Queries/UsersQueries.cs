﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webApplication.Infrastructure;

namespace webApplication.Application.Queries
{
    public class UsersQueries : IUsersQueries
    {
        private readonly Context _context;

        public UsersQueries(Context context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            return await _context.Users
                .Select(u => new User{
                                    Id = u.Id,
                                    FIO = u.FIO,
                                    MobileNumber = u.MobileNumber,
                                    Birthday = u.Birthday.ToShortDateString()                   
                                    } 
                        )
                        .AsNoTracking()
                        .ToListAsync();
        }
    }
}
