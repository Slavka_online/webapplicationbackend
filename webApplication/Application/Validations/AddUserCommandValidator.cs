﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webApplication.Application.Commands;

namespace webApplication.Application.Validations
{
    public class AddUserCommandValidator : AbstractValidator<AddUserCommand>
    {
        public AddUserCommandValidator()
        {
            RuleFor(command => command.FIO)
                .NotNull()
                .NotEmpty()
                .MaximumLength(100);
            RuleFor(command => command.MobileNumber)
                .NotNull()
                .NotEmpty()
                .MaximumLength(20);

            RuleFor(command => command.Birthday)
                .NotNull()
                .NotEmpty()
                .Must(DateValid)
                .WithMessage("Неверный формат даты")
                .MaximumLength(10);
        }

        private bool DateValid(string date)
        {
            return DateTime.TryParseExact(date, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime result);
        }
    }
}
