﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webApplication.Application.Commands
{
    public class EditUserCommand: IRequest<bool>
    {
        public long Id { get; set; }
        public string FIO { get; set; }
        public string Birthday { get; set; }
        public string MobileNumber { get; set; }
    }
}
