﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using webApplication.Infrastructure;

namespace webApplication.Application.Commands
{
    public class EditUserCommandHandler : IRequestHandler<EditUserCommand, bool>
    {

        private readonly Context _context;

        public EditUserCommandHandler(Context context)
        {
            _context = context;
        }

        public async Task<bool> Handle(EditUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.SingleOrDefaultAsync(u => u.Id == request.Id);
            if(user is null)
            {
                return false;
            }
            user.FIO = request.FIO;
            user.Birthday = DateTime.ParseExact(request.Birthday, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
            user.MobileNumber = request.MobileNumber;
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
