﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using webApplication.Infrastructure;
using webApplication.Models;

namespace webApplication.Application.Commands
{
    public class AddUserCommandHandler : IRequestHandler<AddUserCommand, bool>
    {

        private readonly Context _context;

        public AddUserCommandHandler(Context context)
        {
            _context = context;
        }

        public async Task<bool> Handle(AddUserCommand request, CancellationToken cancellationToken)
        {
            var user = new User
            {
                FIO = request.FIO,
                MobileNumber = request.MobileNumber,
                Birthday = DateTime.ParseExact(request.Birthday, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture)
            };
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
