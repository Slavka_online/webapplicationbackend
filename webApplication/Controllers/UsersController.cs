﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using webApplication.Application.Commands;
using webApplication.Application.Queries;

namespace webApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUsersQueries _usersQueries;

        public UsersController(IMediator mediator, IUsersQueries usersQueries)
        {
            _mediator = mediator;
            _usersQueries = usersQueries;
        }

        [HttpPost]
        public async Task<IActionResult> AddUser([FromBody] AddUserCommand command)
        {
            if(!this.ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _mediator.Send(command);
            return result ? (IActionResult)Ok() : (IActionResult)BadRequest();
        }


        [HttpPut]
        public async Task<IActionResult> EditUser([FromBody] EditUserCommand command)
        {
            if (!this.ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _mediator.Send(command);
            return result ? (IActionResult)Ok() : (IActionResult)BadRequest();
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _usersQueries.GetAllUsers();
            return Ok(users);
        }

    }
}