using System;

namespace webApplication.Models
{
    public class User
    {
        public long Id { get; set; }
        public string FIO { get; set; }
        public DateTime Birthday { get; set; }
        public string MobileNumber { get; set; }
    }
}